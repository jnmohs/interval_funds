
# coding: utf-8

# In[96]:


import plotly.plotly as py
import plotly.graph_objs as go
import plotly.figure_factory as FF
import cufflinks as cf
import numpy as np
import pandas as pd
import seaborn
#import the data
df = pd.read_csv('funds declared effective.csv')  #change this depending on directory where you store csv file


# In[97]:


df=pd.value_counts(df['Quarter'].values, sort=True) #turns the df into a table with #declared effective by quarter


# In[98]:


df=df.sort_index()#sort
#df


# In[111]:


#df.to_csv('fund_launch_quarter.csv')



# In[110]:


df2 = pd.read_csv('fund_launch_quarter.csv', names= ["quarter","funds"])
#df2.columns
#df2


# In[118]:


trace1 = go.Bar(
                    x=df2['quarter'], y=df2['funds'], name='Newly Launched', yaxis='y'# Data
                    
                   )
layout = go.Layout(
    title='Interval Fund Launches', legend=dict(orientation="h"), 
    yaxis=dict(
        title='New Funds Approved By SEC', side ='left', range=[0, 5]    ), xaxis=dict(
        title='Source: Interval Fund Tracker'  )
    
)


# In[117]:


data = [trace1]
fig = go.Figure(data=data, layout= layout)
py.iplot(fig, filename='fund_launches')

