
# coding: utf-8

# In[5]:


#http://www.intervalfundtracker.com
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.figure_factory as FF

import numpy as np
import pandas as pd

#import the data
df = pd.read_csv('fund_registrations.csv')  #change this depending on directory where you store csv file


# In[6]:


#chart data source and layout
trace1 = go.Pie(labels=df['Strategy'])
layout = go.Layout(
    title='Interval Funds in Registration: Percent of Funds by Strategy', legend=dict(orientation="h"), 
   xaxis=dict(
        title='Source: Interval Fund Tracker'  )
    
)


# In[7]:


data = [trace1]
fig = go.Figure(data=data, layout = layout)
py.iplot(fig, filename='fund registrations')

